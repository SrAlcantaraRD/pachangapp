import 'package:flutter/material.dart';
import 'package:pachangapp/screens/login/auth.dart';

class HomePage extends StatelessWidget {
  HomePage({this.auth, this.onSignOut});
  final BaseAuth auth;
  final VoidCallback onSignOut;

  @override
  Widget build(BuildContext context) {
    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }
    }

    return new Scaffold(
        appBar: new AppBar(
          actions: <Widget>[
            new IconButton(
                icon: const Icon(Icons.settings_power), onPressed: _signOut)
          ],
        ),
        drawer: _lateralMenu(context),
        body: new Center(
          child: new Text(
            'Welcome',
            style: new TextStyle(fontSize: 32.0),
          ),
        ));
  }

  Widget _lateralMenu(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: Text('Drawer Header'),
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
        ),
        ListTile(
          title: Text('Locales'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text('Inventario'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text('Empleados'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text('Compras'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text('Ventas'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text('Locales'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text('Productos'),
          onTap: () {
            // Update the state of the app
            // ...
            // Then close the drawer
            Navigator.pop(context);
          },
        ),
      ],
    ));
  }
}
